from pathlib import Path

from django.shortcuts import render
from django.http import HttpResponse, FileResponse
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings

from extpkg.models import (Access, License, Client)

# Create your views here.

def get_python(request, version="", _file=""):
    _access_from(request)
    print("GET {} {}".format(version, _file))
    key = request.headers.get("X-EXTPIP-CLIENTID", "")

    print("HEADERS: {}".format(request.headers))

    try:
        license = License.objects.get(key=key)
    except License.DoesNotExist as e:
        return HttpResponse(status=403)

    python_path = Path(settings.PYTHON_PATH, version, _file)
    if not python_path.exists():
        return HttpResponse(status=404)

    f = python_path.open('rb')
    return FileResponse(f, status=200,
                        as_attachment=True, filename=_file)

def _access_from(request):
    op = request.headers.get("X-EXTPIP-OP", "")
    package = request.headers.get("X-EXTPIP-PACKAGE", "")
    uname = request.headers.get("X-EXTPIP-UNAME", "")
    key = request.headers.get("X-EXTPIP-CLIENTID", "")
    forwarded_fors = [u.strip()
                      for u in
                      request.headers.get("X-FORWARDED-FOR", "").split(",")]

    if len(forwarded_fors) > 1:
        source = forwarded_fors[-2]
    else:
        source = request.META.get("REMOTE_ADDR", "")

    print("New access from op({}) package({}) uname({}) "
          "source({}), key({})".format(op, package, uname, source, key))

    try:
        license = License.objects.get(key=key)
    except License.DoesNotExist as e:
        license = None

    try:
        access = Access.objects.create(
                op=op, package=package, uname=uname, source=source,
                license=license)
        access.save()
    except Exception as e:
        print("Failed to create access: {}".format(e))

@csrf_exempt
def update(request):
    _access_from(request)
    return HttpResponse(status=200)



