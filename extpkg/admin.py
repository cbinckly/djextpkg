from django.contrib import admin
from extpkg import models

# Register your models here.
admin.site.register(models.Access)
admin.site.register(models.ClientCategory)
admin.site.register(models.Client)
admin.site.register(models.License)
