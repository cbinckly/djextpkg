from django.db import models

# Create your models here.

class ClientCategory(models.Model):
    name = models.CharField(max_length=64)

    def __str__(self):
        return self.name

class Client(models.Model):
    first_name = models.CharField(max_length=64)
    last_name = models.CharField(max_length=64)
    business_name = models.CharField(max_length=128)
    address1 = models.CharField(max_length=64)
    address2 = models.CharField(max_length=64)
    address3 = models.CharField(max_length=64)
    phone = models.CharField(max_length=24)
    email = models.CharField(max_length=64)
    category = models.ForeignKey(ClientCategory,
                                 related_name="clients",
                                 on_delete=models.SET_NULL,
                                 null=True)

    def __str__(self):
        return "{} {} ({})".format(self.first_name,
                                   self.last_name,
                                   self.business_name)

class License(models.Model):
    client = models.ForeignKey(Client,
                               related_name="licenses",
                               on_delete=models.SET_NULL,
                               null=True)
    key = models.CharField(max_length=64, unique=True)
    expires_on = models.DateTimeField()

    def __str__(self):
        return "{} - {} - {}".format(self.client, self.key, self.expires_on)

class Access(models.Model):
    license = models.ForeignKey(License,
                                to_field='key',
                                related_name="accesses",
                                on_delete=models.SET_NULL,
                                null=True, blank=True)
    source = models.GenericIPAddressField(protocol="IPv4")
    uname = models.CharField(max_length=512)
    package = models.CharField(max_length=64)
    op = models.CharField(max_length=32)
    on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} - {} - {} {} ({})".format(
                self.license, self.source, self.op, self.package, self.on)

