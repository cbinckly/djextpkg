from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User


class Command(BaseCommand):
    help = 'Create a superuser and allow password to be provided'

    def add_arguments(self, parser):
        parser.add_argument(
                '--username', dest='username', default=None,
                help='Specifies the username for the superuser.',
                )
        parser.add_argument(
                '--email', dest='email', default=None,
                help='Specifies the email for the superuser.',
                )
        parser.add_argument(
                '--password', dest='password', default=None,
                help='Specifies the password for the superuser.',
                )

    def handle(self, *args, **options):
        password = options.get('password')
        username = options.get('username')
        email = options.get('email')

        if not (password and username and email):
            raise CommandError("--username is required if specifying --password")

        self.stdout.write(f"Creating superuser {username} ({email}) ... ")
        user, _c = User.objects.get_or_create(username=username, email=email)
        user.is_superuser = True
        user.is_staff = True
        if _c:
            user.set_password(password)
        user.save()
        self.stdout.write("done.")

