#!/bin/sh

APP_PATH=/usr/src/app
DB_PORT=5432

if [[ -z "$RDS_HOST" ]]; then
    DB_HOST=db
else
    DB_HOST=$RDS_HOST
fi

echo "Waiting for $DB_HOST on $DB_PORT."
echo "Settings: $RDS_HOST:$RDS_PORT $RDS_NAME as $RDS_USER"

while ! nc -w 1 -z $DB_HOST $DB_PORT; do 
    sleep 60; 
    echo "waiting for db."
done; 

cd $APP_PATH
./manage.py migrate
./manage.py createsu --username admin --password admin --email cbinckly@gmail.com
gunicorn djextpkg.wsgi -b 0.0.0.0:8000 --access-logfile=- --error-logfile=- --log-level=INFO
